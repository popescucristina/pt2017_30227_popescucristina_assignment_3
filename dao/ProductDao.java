package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
public class ProductDao{

	private final static Statement myStmt = Jdbc.conectiune();
	private Object[][] datas;
	
	
	public void selectProduct() {
		String product ="select * from product_table";
			try {
				myStmt.executeQuery(product);			
	
				JOptionPane.showMessageDialog(null,"S-a efectuat cu succes afisarea!!!");
				
				ResultSet myRs = myStmt.executeQuery("select * from product_table");
				//
				System.out.println("ID\tNUME\tPRET\t\tBucati\n");
				while (myRs.next()) {
					System.out.println(myRs.getString("id") + "     " + myRs.getString("nume")+ "\t" + myRs.getString("pret")+ "\t\t" + myRs.getString("bucati"));
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
	
	
	public void addProduct(String nume,int pret, int nrbucati) {
	String product ="insert into product_table(nume,pret,depozit_id,bucati) values ('"+nume+"',"+pret+",1,"+nrbucati+")";
		try {
			myStmt.executeUpdate(product);
			JOptionPane.showMessageDialog(null,"S-a adaugat un produs!!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public int detailsProduct(int id) {
		// // String product ="select * from product_table";
		int x = -1;
		
			try {
				//myStmt.executeQuery(product);			
	
				//JOptionPane.showMessageDialog(null,"S-a efectuat cu succes afisarea!!!");
				
				ResultSet myRs = myStmt.executeQuery("select bucati from product_table where id ="+id);
				//
				//System.out.println("ID\tNUME\tPRET\t\tBucati\n");
				//while (myRs.next()) {
				//	System.out.println(myRs.getString("id") + "     " + myRs.getString("nume")+ "\t" + myRs.getString("pret")+ "\t\t" + myRs.getString("bucati"));
				//}
				
				if (myRs.next())  x = Integer.parseInt(myRs.getString("bucati")); 
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return x;
		
		}
	
	public void editProduct(int pret, int id){
		String product ="update product_table set pret ='"+pret+"' where id = "+id;
		try {
			myStmt.executeUpdate(product);
			JOptionPane.showMessageDialog(null,"S-a editat un produs!!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public void updateProduct(int bucati, int id){
		String product ="update product_table set bucati ='"+bucati+"' where id = "+id;
		try {
			myStmt.executeUpdate(product);
			JOptionPane.showMessageDialog(null,"S-a facut update la produsul comandat!!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public void deleteProduct(int id){
		String product ="DELETE FROM product_table where id = " + id;
		try {
			myStmt.executeUpdate(product);
			JOptionPane.showMessageDialog(null,"S-a sters un produs!!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	public Object[][] arrayObject(){
		datas = null;
		
		String product ="select * from product_table";
		try {
			myStmt.executeQuery(product);			

			//JOptionPane.showMessageDialog(null,"S-a efectuat cu succes afisarea!!!");
			
			ResultSet myRs = myStmt.executeQuery("select * from product_table");
			int i = 0;
			while (myRs.next()) {
				//int j = 0;
				//System.out.println(myRs.getString("id") + "     " + myRs.getString("nume")+ "\t" + myRs.getString("pret")+ "\t\t" + myRs.getString("bucati"));
				datas[i][0] = myRs.getString("ID");
				datas[i][1] = myRs.getString("nume");
				datas[i][2] = myRs.getString("pret");
				datas[i][3] = myRs.getString("bucati");
				i++;			
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return datas;		
	}
	
}