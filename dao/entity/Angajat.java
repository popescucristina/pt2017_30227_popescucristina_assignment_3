package dao.entity;

import java.util.ArrayList;

public class Angajat {
	private int id;
	private String nume;
	private int aniVechime;
	private ArrayList<Product> responsabilitate;
	
	public Angajat(int id, String nume,int aniVechime, ArrayList<Product> responsabilitate){
			this.id = id;
			this.nume = nume;
			this.aniVechime = aniVechime;
			this.responsabilitate = responsabilitate;
	}
	
	public int getId(){
		return this.id;
	}
	public String getNume(){                                     
		return this.nume;
	}
	public int getVechime(){
		return this.aniVechime;
	}
	public ArrayList<Product> getResponsab(){
		return this.responsabilitate;
	}
	
}
