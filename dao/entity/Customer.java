package dao.entity;

public class Customer {
	private int id;
	private String nume;
	private String email;
	private String telefon;
	
	public Customer(int id, String nume, String email, String telefon){
		this.id = id;
		this.nume = nume;
		this.email = email;
		this.telefon = telefon;		
	}
	
	public int getId(){
		return this.id;
	}
	public String getNume(){
		return this.nume;
	}
	public String getEmail(){
		return this.email;
	}
	public String getTelefon(){
		return this.telefon;
	}	
	
}