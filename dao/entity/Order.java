package dao.entity;

public class Order {
	private int numar;
	private String customerName;
	
	public Order(){
		
	}
	
	public Order(int numar, String customerName){
		this.numar = numar;
		this.customerName = customerName;
	}
	
	public int getNumar(){
		return this.numar;
	}
	
	public String getCustomerName(){
		return this.customerName;
	}
	
	public String toStrig(){
		String s;
		s = "Comanda numarul "+ numar + "a fost facuta de "+customerName;
		return s;
	}
	
//	public String toString(){
//		return cus.getClientNume()+" are o comanda";
//	}
	
}
