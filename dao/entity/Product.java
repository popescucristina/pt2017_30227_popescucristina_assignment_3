package dao.entity;

public class Product {
	private int id;
	private String nume;
	private int pret;
	private int bucati;
	
	
	public Product(String nume, int pret, int bucati) {
		super();
		this.nume = nume;
		this.pret = pret;
		this.bucati = bucati;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public int getPret() {
		return pret;
	}
	public void setPret(int pret) {
		this.pret = pret;
	}
	public int getBucati() {
		return bucati;
	}
	public void setBucati(int bucati) {
		this.bucati = bucati;
	}
	
	
}
