package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;
public class OrderDao{

	private final static Statement myStmt = Jdbc.conectiune();
	private final static Statement myStmt1 = Jdbc.conectiune();
	
	
	public void selectOrder() {
		String order ="select * from order_table";
			try {
				myStmt.executeQuery(order);			
	
				JOptionPane.showMessageDialog(null,"S-a efectuat cu succes afisarea!!!");
				
				ResultSet myRs = myStmt.executeQuery("select * from order_table");
				//
				System.out.println("ID\tCustomerId\tProductId\t\tBucati\n");
				while (myRs.next()) {
					System.out.println(myRs.getString("id") + "     " + myRs.getString("customer_id")+ "\t" + myRs.getString("product_id")+ "\t\t" + myRs.getString("bucati"));
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
	
	public String printOrder(int id) {
		String s = "\t\t\t\tCHITANTA\n\n";
		 s = s + "Order id : "+id+"\n\n"+"Produsele comandate:";
		 int suma =0;
			try {
				
				ResultSet myRs = myStmt.executeQuery("select * from order_table where id ="+id);	
				
				
				while (myRs.next()) {
					int x = Integer.parseInt(myRs.getString("product_id"));
					int b = Integer.parseInt(myRs.getString("bucati"));
					s=s+"\nprodusul "+ x + "\t\tnr bucati:" + b ;
					//ResultSet myRs1 = myStmt1.executeQuery("Select PRET from product_table where id = "+ x);
					//int pret = Integer.parseInt(myRs1.getString("pret"));
					//suma += pret * b;				
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//	s= s + "\n\n\nSUMA TOTALA : "+ suma;
			return s;
		
		}
	
	
	public void addOrder(int id, int customerId, int productId, int bucati) {
	String order ="insert into order_table(id,customer_id,product_id,bucati) values ("+id+","+customerId+","+productId+","+bucati+")";
		try {
			myStmt.executeUpdate(order);
			JOptionPane.showMessageDialog(null,"S-a adaugat un item la comanda!!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	
	public void editOrder(int id, int customerId,int productId, int bucati){
		String order ="update order_table set bucati ='"+bucati+"' where product_id = "+productId+" and id = "+id+ " and customer_id = "+customerId;
		try {
			myStmt.executeUpdate(order);
			JOptionPane.showMessageDialog(null,"S-a editat un item din comanda!!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public void deleteOrder(int id){
		String order ="DELETE FROM order_table where id = " + id;
		try {
			myStmt.executeUpdate(order);
			JOptionPane.showMessageDialog(null,"S-a sters un item din comanda!!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	public Object[][] arrayObject(){
		Object[][] datas = null;
		
		String order ="select * from order_table";
		try {
			myStmt.executeQuery(order);			

			//JOptionPane.showMessageDialog(null,"S-a efectuat cu succes afisarea!!!");
			
			ResultSet myRs = myStmt.executeQuery("select * from order_table");
			int i = 0;
			while (myRs.next()) {
				//int j = 0;
				//System.out.println(myRs.getString("id") + "     " + myRs.getString("customer_id")+ "\t" + myRs.getString("product_id")+ "\t\t" + myRs.getString("bucati"));
				datas[i][0] = myRs.getString("id");
				datas[i][1] = myRs.getString("customer_id");
				datas[i][2] = myRs.getString("product_id");
				datas[i][3] = myRs.getString("bucati");
				i++;			
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return datas;		
	}
}
