package presentation;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Comand {
	View v;
	public Comand(View v){
		this.v = v;
		//v.showAdunareListener(new AddListenerAdunare());
		v.clientListener(new AddListenerClient());
		v.adminListener(new AddListenerAdmin());
	}

	class AddListenerClient implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("comanda");
			ViewComand vc = new ViewComand();
			ComandComand cc = new ComandComand(vc);
		}		
	}
	class AddListenerAdmin implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("admin");	
		
			ViewAdmin va = new ViewAdmin();
			ComandAdmin ca = new ComandAdmin(va);
		}		
	}
	
}