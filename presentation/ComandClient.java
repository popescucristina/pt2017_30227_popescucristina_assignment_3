package presentation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import businessLogic.ProductBusinessLogic;
import dao.ProductDao;


public class ComandClient {
	ViewClientOp vpo;
//	ProductBusinessLogic productLogic = new ProductBusinessLogic();
//	String[] columnNames = {"ID",
//            "NumeClient",
//            "Email",
//            "NumarTelefon"};
	public ComandClient(ViewClientOp vpo){
		this.vpo = vpo;
		Object[][] data= null;
		
		//data = 
		vpo.addListener(new AddListenerAdd());
		vpo.editListener(new AddListenerEdit());
		vpo.deleteListener(new AddListenerDelete());
		vpo.selectCustomer();
		//vpo.addInJTable(columnNames, data);
		
	}
		
	class AddListenerAdd implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			System.out.println("add");	
			vpo.addCustomer();
			vpo.selectCustomer();
			//String[] columnNames= null;
//			Object[][] data = null;
//			data = productLogic.arrayObject();
//			
//			vpo.addInJTable(columnNames,data);
			
		}		
	}
	
	class AddListenerEdit implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("edit");	
			vpo.editCustomer();
			vpo.selectCustomer();
			//String[] columnNames = null;
//			Object[][] data= null;
//			data = productLogic.arrayObject();
//			vpo.addInJTable(columnNames,data);
		}		
	}
	
	class AddListenerDelete implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("delete");
			vpo.deleteCustomer();
			vpo.selectCustomer();
			//String[] columnNames = null;
//			Object[][] data = null;
//			data = productLogic.arrayObject();
//			vpo.addInJTable(columnNames,data);
		}		
	}
}