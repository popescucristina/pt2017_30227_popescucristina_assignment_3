package presentation;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.*;

import businessLogic.CustomerBusinessLogic;

public class ViewClientOp extends JFrame{
	

	private static final long serialVersionUID = 1L;
	private JPanel mainPanel = new JPanel();
	
	private static JLabel label = new JLabel();
	private static JLabel label1 = new JLabel();
	private static JLabel label2 = new JLabel();
	private static JLabel label3 = new JLabel();
	
	//private static JLabel label1 = new JLabel();
	private static JLabel bigLabel = new JLabel();
	
	private static JButton add = new JButton("ADD");
	private static JButton edit= new JButton("EDIT");
	private static JButton delete = new JButton("DELETE");
	
	private static JTextField textid = new JTextField();
	private static JTextField textnume = new JTextField();
	private static JTextField textemail= new JTextField();
	private static JTextField texttelefon= new JTextField();
	private static JTextField textedit = new JTextField("   New email");
//	private static String[] columnNames= {"Id", "Nume", "e-mail","telefon"};
//	private static Object[][] data={};
//	private static JTable tabel = new JTable(data,columnNames);
//	
//	
	CustomerBusinessLogic customerLogic= new CustomerBusinessLogic();
	public ViewClientOp(){
		add(mainPanel);
		mainPanel.setLayout(null);
		addComponents();
		jFrameSetup();
				
	}
	
	private void jFrameSetup(){
		setTitle("Popescu Cristina-Maria");
		setSize(1150,900);
		
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE); 		
	}
	
	private void addComponents(){
		/////
		bigLabel.setText("Client Operation");
		bigLabel.setBounds(70, 20, 1000,100);
		bigLabel.setFont(new Font("Impact", 30, 100));
		bigLabel.setHorizontalAlignment(SwingConstants.CENTER);
		mainPanel.add(bigLabel);
		/////
		
		label.setText("Id");
		label.setBounds(150, 200, 700,30);
		label.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label);	
				
			
		label1.setText("Nume");
		label1.setBounds(150, 250, 700,30);
		label1.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label1);
		
		label2.setText("Email");
		label2.setBounds(150, 300, 700,30);
		label2.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label2);
		
		label3.setText("Telefon");
		label3.setBounds(150, 350, 700,30);
		label3.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label3);
		
		textid.setBounds(275,200,20,20);
		textid.setSize(50, 30);
		textid.setFont(new Font("Forte", 30,20));
		mainPanel.add(textid);
		
		
		textnume.setBounds(275,250,20,20);
		textnume.setSize(200, 30);
		textnume.setFont(new Font("Forte", 30,20));
		mainPanel.add(textnume);
		
		
		textemail.setBounds(275,300,20,20);
		textemail.setSize(200, 30);
		textemail.setFont(new Font("Forte", 30,20));
		mainPanel.add(textemail);
		
		texttelefon.setBounds(275,350,20,20);
		texttelefon.setSize(200, 30);
		texttelefon.setFont(new Font("Forte", 30,20));
		mainPanel.add(texttelefon);
		
		
		add.setBounds(500,200,130,130);
		mainPanel.add(add);
		
		edit.setBounds(700,200,130,130);
		mainPanel.add(edit);
		
		textedit.setBounds(700,350,20,20);
		textedit.setSize(130, 30);
		textedit.setFont(new Font("Forte", 30,20));
		mainPanel.add(textedit);
		
		delete.setBounds(900,200,130,130);
		mainPanel.add(delete);
		
		addInJTable();
		
//		tabel.setPreferredScrollableViewportSize(new Dimension(500,50));
//		tabel.setFillsViewportHeight(true);
//		JScrollPane sP = new JScrollPane(tabel);
//		mainPanel.add(tabel);
//		mainPanel.add(sP);
		
		

	}
//action listener pe buton
	
	public void addListener(ActionListener a){
		add.addActionListener(a);
	}
	public void editListener(ActionListener a){
		edit.addActionListener(a);
	}
	public void deleteListener(ActionListener a){
		delete.addActionListener(a);
	}

	
//get smth from gui
	public int getID(){
		return Integer.parseInt(textid.getText());
	}

	public String getNume(){
		return textnume.getText();
	}
	
	public String getEmail(){
		return textemail.getText();
	}
	
	public String getTelefon(){
		return texttelefon.getText();
	}
	
	public void selectCustomer(){
		customerLogic.selectCustomer();
	}
	
	public void addCustomer(){
		//System.out.println(textnume.getText());
		customerLogic.addCustomer(textnume.getText(),textemail.getText(),texttelefon.getText());
	}
	
	public void editCustomer(){
		//System.out.println(textnume.getText());
		customerLogic.editCustomer(textedit.getText(),Integer.parseInt(textid.getText()));
	}
	
	public void deleteCustomer(){
		//System.out.println(textnume.getText());
		customerLogic.deleteCustomer(Integer.parseInt(textid.getText()));
	}
	
	public void addInJTable(){
		String[] columnNames = {"ID",
      "NumeProdus",
      "Pret",
      "DepozitId",
      "NumarBucati"};
		
		Object[][] data = {
			    {"Id", "NumeProdus",
			     "Pret", "DepozitId", "NumarBucati"},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""},
			    {"", "", "", "", ""}
			};
		
		JTable table = new JTable(data,columnNames);
		table.setBounds(70,500 , 500, 500);
		mainPanel.add(table);
		
		
	}

}

