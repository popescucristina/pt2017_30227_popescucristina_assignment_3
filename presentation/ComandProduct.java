package presentation;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import businessLogic.ProductBusinessLogic;


public class ComandProduct {
	ViewProductOp vpo;

	//ProductBusinessLogic productLogic = new ProductBusinessLogic();
//	String[] columnNames = {"ID",
//            "NumeProdus",
//            "Pret",
//            "DepozitId",
//            "NumarBucati"};
	public ComandProduct(ViewProductOp vpo){
		this.vpo = vpo;
		vpo.addListener(new AddListenerAdd());
		vpo.editListener(new AddListenerEdit());
		vpo.deleteListener(new AddListenerDelete());
		vpo.selectProduct();
		
	}
		
	class AddListenerAdd implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("add");
			vpo.addProduct();
			vpo.selectProduct();
			//String[] columnNames = null;
			//Object[][] data= null;
			
			//data = productLogic.arrayObject();
			//vpo.addInJTable(columnNames, data);
		}		
	}
	
	class AddListenerEdit implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("edit");	
			vpo.editProduct();
			vpo.selectProduct();
			//String[] columnNames = null;
			//Object[][] data= null;
			//data = productLogic.arrayObject();
			//vpo.addInJTable(columnNames, data);
		}		
	}
	
	class AddListenerDelete implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("delete");
			vpo.deleteProduct();
			vpo.selectProduct();
			//String[] columnNames = null;
//			Object[][] data= null;
//			data = productLogic.arrayObject();
//			vpo.addInJTable(columnNames, data);
		}		
	}
}