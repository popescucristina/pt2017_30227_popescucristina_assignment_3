package presentation;


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import javax.swing.*;

//import domain.View;

public class ViewAdmin extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel = new JPanel();
	
	private static JLabel label = new JLabel();
	
	private static JLabel bigLabel = new JLabel();
	
	private static JButton client = new JButton("Client");
	private static JButton produs = new JButton("Produs");
	
	public ViewAdmin(){
		add(mainPanel);
		mainPanel.setLayout(null);
		addComponents();
		jFrameSetup();
		mainPanel.setBackground(Color.yellow);
				
	}
	
	private void jFrameSetup(){
		setTitle("Popescu Cristina-Maria");
		setSize(600,600);
		
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE); 		
	}
	
	private void addComponents(){
		bigLabel.setText("Baze de Date");
		bigLabel.setBounds(50, 20, 500,100);
		bigLabel.setFont(new Font("Impact", 30, 50));
		bigLabel.setHorizontalAlignment(SwingConstants.CENTER);
		mainPanel.add(bigLabel);	
		
		label.setText("Doriti sa faceti operatii pe Client sau pe Produs?");
		label.setBounds(50, 100, 500,100);
		label.setFont(new Font("Impact", 30, 20));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		mainPanel.add(label);	
		
		client.setBounds(350,250,150,150);
		mainPanel.add(client);
		
		produs.setBounds(100,250,150,150);
		mainPanel.add(produs);
		

	}
//action listener pe buton
	public void clientListener(ActionListener a){
		client.addActionListener(a);
		
	}public void produsListener(ActionListener a){
		produs.addActionListener(a);
	}
}
