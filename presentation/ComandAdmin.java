package presentation;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ComandAdmin {
	ViewAdmin va;
	public ComandAdmin(ViewAdmin va){
		this.va = va;
		//v.showAdunareListener(new AddListenerAdunare());
		va.clientListener(new AddListenerClient());
		va.produsListener(new AddListenerProdus());
	}

	class AddListenerClient implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("client");
			ViewClientOp vco = new ViewClientOp();
			ComandClient cc = new ComandClient(vco);
		}		
	}
	class AddListenerProdus implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("produs");	
			ViewProductOp vpo = new ViewProductOp();
			ComandProduct cp = new ComandProduct(vpo);
		}		
	}
	
}