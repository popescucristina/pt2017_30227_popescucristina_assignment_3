package presentation;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.swing.*;

import businessLogic.OrderBusinessLogic;

//import domain.View;

public class ViewComand extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel = new JPanel();
	
	private static JLabel label = new JLabel();
	private static JLabel label1 = new JLabel();
	private static JLabel label2 = new JLabel();
	private static JLabel label3 = new JLabel();
	//private static JLabel label1 = new JLabel();
	private static JLabel bigLabel = new JLabel();
	
	private static JButton add = new JButton("ADD");
	private static JButton edit= new JButton("EDIT");
	private static JButton delete = new JButton("DELETE");
	private static JButton print = new JButton("bill");
	
	private static JTextField textid = new JTextField();
	private static JTextField textnume = new JTextField();
	private static JTextField textedit = new JTextField("Nr bucati(edit)");
	private static JTextField textcustomerid = new JTextField();
	private static JTextField textproductid = new JTextField();
	
	//JScrollPane scrollPane = new JScrollPane(table);
	
	
	
	private static JTextField textbucati = new JTextField();
	OrderBusinessLogic orderLogic = new OrderBusinessLogic();
	
	public ViewComand(){
		add(mainPanel);
		mainPanel.setLayout(null);
		
		addComponents();
		jFrameSetup();
				
	}
	
	private void jFrameSetup(){
		setTitle("Popescu Cristina-Maria");
		setSize(1150,900);
		
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE); 		
	}
	
	private void addComponents(){
		/////
		
		
		
		bigLabel.setText("ORDER");
		bigLabel.setBounds(70, 20, 1000,100);
		bigLabel.setFont(new Font("Impact", 30, 100));
		bigLabel.setHorizontalAlignment(SwingConstants.CENTER);
		mainPanel.add(bigLabel);
		/////
		
		label.setText("Id-ul comenzii: ");
		label.setBounds(150, 200, 700,30);
		label.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label);	
				
			
		label1.setText("Numar bucati:");
		label1.setBounds(150, 250, 700,30);
		label1.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label1);
		
		label2.setText("Customer ID:");
		label2.setBounds(150, 300, 700,30);
		label2.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label2);
		
		label3.setText("ProductId");
		label3.setBounds(150, 350, 700,30);
		label3.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label3);
		
		textid.setBounds(400,200,20,20);
		textid.setSize(50, 30);
		textid.setFont(new Font("Forte", 30,20));
		mainPanel.add(textid);
			
//		
		textcustomerid.setBounds(400,300,20,20);
		textcustomerid.setSize(50, 30);
		textcustomerid.setFont(new Font("Forte", 30,20));
		mainPanel.add(textcustomerid);
		
		textproductid.setBounds(400,350,20,20);
		textproductid.setSize(50, 30);
		textproductid.setFont(new Font("Forte", 30,20));
		mainPanel.add(textproductid);
		
		textbucati.setBounds(400,250,20,20);
		textbucati.setSize(50, 30);
		textbucati.setFont(new Font("Forte", 30,20));
		mainPanel.add(textbucati);
		
		add.setBounds(500,200,130,130);
		mainPanel.add(add);
		
		edit.setBounds(700,200,130,130);
		mainPanel.add(edit);
		
		textedit.setBounds(700,350,20,20);
		textedit.setSize(130, 30);
		textedit.setFont(new Font("Forte", 30,20));
		mainPanel.add(textedit);
		
		delete.setBounds(900,200,130,130);
		mainPanel.add(delete);
		
		print.setBounds(1070, 200, 50, 130);
		mainPanel.add(print);
		addInJTable();
		

	}
//action listener pe buton	
	public void addListener(ActionListener a){
		add.addActionListener(a);
	}
	public void editListener(ActionListener a){
		edit.addActionListener(a);
	}
	public void deleteListener(ActionListener a){
		delete.addActionListener(a);
	}
	public void printListener(ActionListener a){
		print.addActionListener(a);
	}
//get smth from gui 	

	public String getNume(){
		return textnume.getText();
	}
	
	public int getId(){
		return Integer.parseInt(textid.getText());
	}
	public int getBucati(){
		return Integer.parseInt(textbucati.getText());
	}
	public int getCustomerId(){
		return Integer.parseInt(textcustomerid.getText());
	}
	public int getProductId(){
		return Integer.parseInt(textproductid.getText());
	}
	public int getEdit(){
		return Integer.parseInt(textedit.getText());
	}
	
	public void selectOrder(){
		orderLogic.selectOrder();
	}
	
	
	public void addOrder(){
		orderLogic.addOrder(getId(), getCustomerId(), getProductId(),getBucati());
	}
	
	public void editOrder(){
		orderLogic.editOrder(getId(),getCustomerId(),getProductId(),getEdit());
	}
	
	public void deleteOrder(){
		orderLogic.deleteOrder(getId());
	}
	public void printOrder() throws UnsupportedEncodingException, FileNotFoundException, IOException{
		orderLogic.printOrder(getId());
	}
	
//	public void tableOrder(){
//		orderLogic.tableOrder();
//	}
	
	public void addInJTable(){
		String[] columnNames = {"ID",
      "CustomerId",
      "ProductId",
      "NumarBucati"};
		
		Object[][] data = {
			    {"Id", "CustomerId",
			        "ProductId",
			        "NumarBucati"},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""},
			    {"", "", "", ""}
			};
		
		JTable table = new JTable(data,columnNames);
		table.setBounds(70,500 , 500, 500);
		mainPanel.add(table);
		
		
	}
}

