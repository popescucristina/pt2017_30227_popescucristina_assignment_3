package presentation;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;


public class ComandComand {
	ViewComand vc;
//	String[] columnNames = {"ID",
//            "CustomerId",
//            "ProductId",
//            "NumarBucati"};
	public ComandComand(ViewComand vc){
		this.vc = vc;
		vc.addListener(new AddListenerAdd());
		vc.editListener(new AddListenerEdit());
		vc.deleteListener(new AddListenerDelete());
		vc.printListener(new AddListenerPrint());
		vc.selectOrder();
		
	}
	
	class AddListenerAdd implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("add");
			vc.addOrder();
			vc.selectOrder();
			//String[] columnNames = null;
//			Object[][] data= null;
//			vc.addInJTable(columnNames, data);
		}		
	}
	
	class AddListenerEdit implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("edit");	
			vc.editOrder();
			vc.selectOrder();
			//String[] columnNames = null;
//			Object[][] data= null;
//			vc.addInJTable(columnNames, data);
		}		
	}
	
	class AddListenerDelete implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("delete");
			vc.deleteOrder();
			vc.selectOrder();
			//String[] columnNames = null;
//			Object[][] data= null;
//			vc.addInJTable(columnNames, data);
		}		
	}
	
	class AddListenerPrint implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("print");
			try {
				vc.printOrder();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		}		
	}

}