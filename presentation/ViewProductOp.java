package presentation;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.*;

import businessLogic.ProductBusinessLogic;

//import domain.View;

public class ViewProductOp extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel = new JPanel();
	
	private static JLabel label = new JLabel();
	private static JLabel label1 = new JLabel();
	private static JLabel label2 = new JLabel();
	private static JLabel label3 = new JLabel();
	//private static JLabel label1 = new JLabel();
	private static JLabel bigLabel = new JLabel();
	
	private static JButton add = new JButton("ADD");
	private static JButton edit= new JButton("EDIT");
	private static JButton delete = new JButton("DELETE");
	
	private static JTextField textid = new JTextField();
	private static JTextField textnume = new JTextField();
	private static JTextField textpret = new JTextField();
	private static JTextField textedit = new JTextField("New price");
	private static JTextField textbucati = new JTextField();
	ProductBusinessLogic productLogic = new ProductBusinessLogic();
	
	public ViewProductOp(){
		add(mainPanel);
		mainPanel.setLayout(null);
		addComponents();
		jFrameSetup();
				
	}
	
	private void jFrameSetup(){
		setTitle("Popescu Cristina-Maria");
		setSize(1150,900);
		
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE); 		
	}
	
	private void addComponents(){
		/////
		bigLabel.setText("Product Operation");
		bigLabel.setBounds(70, 20, 1000,100);
		bigLabel.setFont(new Font("Impact", 30, 100));
		bigLabel.setHorizontalAlignment(SwingConstants.CENTER);
		mainPanel.add(bigLabel);
		/////
		
		label.setText("Id");
		label.setBounds(150, 200, 700,30);
		label.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label);	
				
			
		label1.setText("Nume");
		label1.setBounds(150, 250, 700,30);
		label1.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label1);
		
		label2.setText("Pret");
		label2.setBounds(150, 300, 700,30);
		label2.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label2);
		
		label3.setText("Bucati");
		label3.setBounds(150, 350, 700,30);
		label3.setFont(new Font("Forte", 30, 30));
		mainPanel.add(label3);
		
		textid.setBounds(250,200,20,20);
		textid.setSize(50, 30);
		textid.setFont(new Font("Forte", 30,20));
		mainPanel.add(textid);
		
		
		textnume.setBounds(250,250,20,20);
		textnume.setSize(200, 30);
		textnume.setFont(new Font("Forte", 30,20));
		mainPanel.add(textnume);
		
		
		textpret.setBounds(250,300,20,20);
		textpret.setSize(100, 30);
		textpret.setFont(new Font("Forte", 30,20));
		mainPanel.add(textpret);
		
		textbucati.setBounds(250,350,20,20);
		textbucati.setSize(100, 30);
		textbucati.setFont(new Font("Forte", 30,20));
		mainPanel.add(textbucati);
		
		add.setBounds(500,200,130,130);
		mainPanel.add(add);
		
		edit.setBounds(700,200,130,130);
		mainPanel.add(edit);
		textedit.setBounds(700,350,20,20);
		textedit.setSize(130, 30);
		textedit.setFont(new Font("Forte", 30,20));
		mainPanel.add(textedit);
		
		delete.setBounds(900,200,130,130);
		mainPanel.add(delete);
		addInJTable();
		

	}
//action listener pe buton	
	public void addListener(ActionListener a){
		add.addActionListener(a);
	}
	public void editListener(ActionListener a){
		edit.addActionListener(a);
	}
	public void deleteListener(ActionListener a){
		delete.addActionListener(a);
	}

//get smth from gui 	
	public int getPol1(){
		return Integer.parseInt(textid.getText());
	}
	
	public String getNume(){
		return textnume.getText();
	}
	
	public int getPret(){
		return Integer.parseInt(textpret.getText());
	}	
	
	public int getId(){
		return Integer.parseInt(textid.getText());
	}
	public int getBucati(){
		return Integer.parseInt(textbucati.getText());
	}
	
	public void selectProduct(){
		productLogic.selectProduct();
	}
	
	public void addProduct(){
		productLogic.addProduct(getNume(),getPret(),getBucati());
	}
	
	public void editProduct(){
		productLogic.editProduct(getPret(), getId());
	}
	
	public void deleteProduct(){
		productLogic.deleteProduct(getId());
	}

	public void addInJTable(){
		String[] columnNames = {"ID",
      "NumeProdus",
      "Pret",
      "DepozitId",
      "NumarBucati"};
		
	Object[][] data = {
					    {"Id", "NumeProdus",
					        "Pret",
					        "DepozitId",
					        "NumarBucati"},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""},
					    {"", "", "", "", ""}
					};
		
		JTable table = new JTable(data,columnNames);
		table.setBounds(70,500 , 500, 500);
		mainPanel.add(table);
		
		
	}

}

