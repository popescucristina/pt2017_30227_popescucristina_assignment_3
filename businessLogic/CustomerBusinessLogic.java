package businessLogic;

import dao.CustomerDao;

public class CustomerBusinessLogic{

	
	private final CustomerDao customerDao = new CustomerDao();
	
	public CustomerBusinessLogic(){
		//this.customerDao=customerDao;
	}
	
	public void selectCustomer(){
		System.out.println("intra");
		
		customerDao.selectCustomer();
	}
	
	
	public void addCustomer(String nume, String email, String telefon) {
		System.out.println("intra");
	
		customerDao.addCustomer(nume,email,telefon);
	}

	public void editCustomer(String email, int id) {
		System.out.println("intra");
	
		customerDao.editCustomer(email,id);
	}
	
	public void deleteCustomer(int id) {
		System.out.println("intra");
		customerDao.deleteCustomer(id);
	}
	
	public Object[][] arrayObject(){
		return customerDao.arrayObject();
	}
}
