package businessLogic;
import dao.ProductDao;

public class ProductBusinessLogic{

	
	private final ProductDao productDao = new ProductDao();
	
	public ProductBusinessLogic(){
		//this.customerDao=customerDao;
	}
	
	public void selectProduct(){
		System.out.println("intra");
		
		productDao.selectProduct();
	}
	
	public void addProduct(String nume,int pret, int bucati) {
		System.out.println("intra");
	
		productDao.addProduct(nume,pret, bucati);
		
	}

	public void editProduct(int pret, int id) {
		System.out.println("intra");
	
		productDao.editProduct(pret,id);
	}
	
	public void deleteProduct(int id) {
		System.out.println("intra");
		productDao.deleteProduct(id);
	}
	public Object[][] arrayObject(){
		return productDao.arrayObject();
	}
}

