package businessLogic;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.swing.JOptionPane;

import dao.OrderDao;
import dao.ProductDao;

public class OrderBusinessLogic{

	
	private final OrderDao orderDao = new OrderDao();
	private final ProductDao productDao = new ProductDao();
	
	public OrderBusinessLogic(){
	}
	public void selectOrder(){		
		orderDao.selectOrder();
	}
	
	public void addOrder(int id, int customerId, int productId, int bucati) {
		int x = productDao.detailsProduct(productId);
		
		if (x-bucati >= 0){
		orderDao.addOrder(id,customerId, productId, bucati);
		productDao.updateProduct(x-bucati, productId);
		JOptionPane.showMessageDialog(null,"E ok");
		}
		else {
			JOptionPane.showMessageDialog(null,"Under stock!");
			}
	}

	public void editOrder(int id,int customerId, int productId, int bucati) {
		orderDao.editOrder(id,customerId, productId, bucati);
	}
	
	public void deleteOrder(int id) {
		orderDao.deleteOrder(id);
	}
	public void printOrder(int id) throws UnsupportedEncodingException, FileNotFoundException, IOException{
		Printer p = new Printer(id);
	}
	public Object[][] arrayObject(){
		return orderDao.arrayObject();
	}
}
